import datetime
import calendar
import pandas as pd
import numpy as np
from datetime import date, timedelta


month=int(input('Enter the Month :'))
Contact= ["De Gruyter Poland Sp z oo"]
Total=['635.8']
z=int(input("ROWS : "))
df=pd.read_csv('March.csv')


def invoiceDatefn(month):
  currentDateTime = datetime.datetime.now()
  date = currentDateTime.date()
  year = date.strftime("%Y")
  monthrange=calendar.monthrange(int(year),month)
  Firstdate,Lastdate=monthrange
  if(month<10):
     Invoice="{}-0{}-{}".format(Lastdate,month,int(year))
  else:
     Invoice="{}-{}-{}".format(Lastdate,month,int(year))
  return(Invoice)

  
def dueDatefn(ID):
  date = pd.to_datetime(ID)
  days_after = (date+timedelta(days=30))
  return days_after

def invoicenumber(month):
 currentDateTime = datetime.datetime.now()
 date = currentDateTime.date()
 year = date.strftime("%Y")
 current_year = date.today().year
 MonthName = calendar.month_name[month]
 b=['IN',year, MonthName,'9534']
 a="-".join(map(str,b))
 return a

def loop(month,K,Contact,Total):
  ID=invoiceDatefn(month)
  Contact=Contact*K
  Total=Total*K
  InvoiceDate=[ID]*K
  DueDate=[dueDatefn(ID)]*K
  InvoiceNumber=[invoicenumber(month)]*K
  dict1={"Contact Name" : Contact,'Email': '','PO AddressLine1' : '','PO AddressLine2' : '', 
         'PO AddressLine3' : '', 'PO AddressLine4' : '','PO City' : '','PO Region' : '',
         'PO PostalCode' : '','PO Country' : '',"Invoice Date" : InvoiceDate,'InvoiceNumber' : InvoiceNumber,
         "Due Date" : DueDate,"Total" : Total,'InventoryItemCode' :'','Discount' : '',
         'TaxAmount' : ''}
  df3=pd.DataFrame(dict1)
  return df3
df3=loop(month,z,Contact,Total) 

def description(df):
  df.columns = df.iloc[0]
  df.drop(index=df.index[0],axis=0,inplace=True)
  df = df[['S. No','Code', 'File Name','Article Name']]
  df1 = (df.set_index(["S. No"]).stack().reset_index(name='Description')
         .rename(columns={'level_2':'Cols'}))
  pv=[]
  k=len(df1)
  df1=df1['Description']
  for i in range(2,k,3):
    df1.loc[i+0.5]='Language'
  df2=df1.sort_index()
  for i in range(0,248,3):
     if(df2.loc[i]==df2.loc[i+3]):      
        pv.append(i+3)
  df2.drop([float(i) for i in pv],axis=0,inplace=True)
  df2=df2.reset_index(drop=True)
  df3["Description"]=df2
  df3.reset_index(drop=True, inplace=True)
  return df3
df2=description(df)


def reference(mon):
  df=pd.DataFrame()
  current_year = date.today().year
  MonthName = calendar.month_name[mon]
  refcol=[MonthName, current_year, "projects"]
  Reference=" ".join(map(str,refcol))
  df3["Reference"]=Reference
reference(month)


def quantity():
  df3['Quantity']=''
  a=df['Page Count (250 words per page)'].astype(float).apply(np.ceil)
  a=[i for i in a]
  a.pop()
  df3.loc[df3['Description']=='Language','Quantity']=a
quantity()



column=df.Amount      
listsize=len(column)-1        


def unitAmount(listsize):
   df3['UnitAmount']=''
   unitamount=[0.85]*listsize
   df3.loc[df3['Description']=='Language','UnitAmount']=unitamount
unitAmount(listsize)

def accountCode(listsize):
   df3['AccountCode']=''
   accountcode=[200]*listsize
   df3.loc[df3['Description']=='Language','AccountCode']=accountcode
accountCode(listsize)


def trackingName1(listsize):
   df3['Tracking Name 1']=''
   tracking=['Job Type']*listsize
   df3.loc[df3['Description']=='Language','Tracking Name 1']=tracking
trackingName1(listsize)

def taxType(listsize):
   df3['TaxType']=''
   tax=['Tax Exempt (0%)']*listsize
   df3.loc[df3['Description']=='Language','TaxType']=tax
taxType(listsize)

def trackingOption1(listsize):
   df3['Tracking Option 1']=''
   trackoption=['Editorial copyediting journals']*listsize
   df3.loc[df3['Description']=='Language','Tracking Option 1']=trackoption
trackingOption1(listsize)

def trackingName2(listsize):
   df3['Tracking Name 2']=''
   trackname2=['Charge code']*listsize
   df3.loc[df3['Description']=='Language','Tracking Name 2']=trackname2
trackingName2(listsize)

def trackingOption2(listsize):
   df3['Tracking Option 2']=''
   trackoption2=['Editorial']*listsize
   df3.loc[df3['Description']=='Language','Tracking Option 2']=trackoption2
trackingOption2(listsize)

df3=df3.loc[:, ['Contact Name','Email','PO AddressLine1','PO AddressLine2', 
         'PO AddressLine3', 'PO AddressLine4','PO City','PO Region',
         'PO PostalCode','PO Country','InvoiceNumber','Reference',"Invoice Date",
         "Due Date",'Total','InventoryItemCode','Description','Quantity','UnitAmount',
          'Discount','AccountCode','TaxType','TaxAmount','Tracking Name 1',
          'Tracking Option 1','Tracking Name 2','Tracking Option 2']]
df3
df3.to_csv('Final.csv',index=False)